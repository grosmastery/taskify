from django.contrib import admin
from core.models import Goals, ToDoList


admin.site.register(Goals)
admin.site.register(ToDoList)
