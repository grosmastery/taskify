from django.db import models
from django.contrib.auth import get_user_model


class ToDoList(models.Model):
    title = models.CharField(max_length=100)
    completed = models.BooleanField(default=False)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    goal = models.ManyToManyField('Goals', blank=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Goals(models.Model):
    PRIORITY_CHOICES = (
        ('low_priority', 'Minor'),
        ('major_priority', 'Major'),
        ('severe_priority', 'Severe'),
    )

    goal = models.CharField(max_length=255)
    priority = models.CharField(max_length=20, choices=PRIORITY_CHOICES, default='low_priority')
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.name




