from django.shortcuts import render
from core.models import Goals, ToDoList
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView, FormView, View
from django.contrib.auth.mixins import LoginRequiredMixin
from core.forms import SignUpForm, CreateGoalsForm
from django.http import JsonResponse


class MainView(LoginRequiredMixin, TemplateView):
    template_name = 'main.html'
    login_url = '/login/'


class SignUpView(FormView):
    template_name = 'signup.html'
    form_class = SignUpForm
    success_url = '/login/'

    def form_valid(self, form):
        form.save()
        return super(SignUpView, self).form_valid(form)


class ToDoListView(LoginRequiredMixin, ListView):
    model = ToDoList
    template_name = 'todo_list.html'
    context_object_name = 'objects'
    login_url = '/login/'

    def get_queryset(self):
        return ToDoList.objects.filter(user=self.request.user)


class ToDoListDetailView(LoginRequiredMixin, DetailView):
    model = ToDoList
    template_name = 'todo_list_detail.html'
    context_object_name = 'object'
    login_url = '/login/'

    def post(self, request, *args, **kwargs):
        goal_id = request.POST['goal_id']
        goal = Goals.objects.filter(id=goal_id).first()
        if goal.completed:
            goal.completed = False
        else:
            goal.completed = True
        goal.save()
        return JsonResponse({'status': 'success'})


class CreateToDoListView(View):

    def post(self, request, *args, **kwargs):
        title = request.POST['task_title']
        task = ToDoList.objects.create(title=title, user=request.user)
        return JsonResponse({'status': 'success', 'task_id': task.id})


class CreateGoalsView(LoginRequiredMixin, FormView):
    form_class = CreateGoalsForm
    template_name = 'create_goals.html'
    success_url = '.'
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['todo_list'] = ToDoList.objects.filter(id=self.kwargs['id']).first()
        return context

    def form_valid(self, form):
        ToDoList.objects.filter(id=self.kwargs['id']).first().goal.add(form.save())
        return super(CreateGoalsView, self).form_valid(form)


class DeleteGoalView(View):

    def post(self, request, *args, **kwargs):
        goal_id = request.POST['goal_id']
        Goals.objects.filter(id=goal_id).delete()
        return JsonResponse({'status': 'success'})




