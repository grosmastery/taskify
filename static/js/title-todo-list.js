$(document).ready(function() {
    $('#submit-title').click(function(e) {
        e.preventDefault();
        let task_title = $('#task-title').val()
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let url = $('#title-form').attr("action");

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'task_title': task_title,
                csrfmiddlewaretoken: token,
            },
            success: function (response) {
                if (response.status == 'success') {
                    window.location.replace(`/creategoal/${response.task_id}/`)
                }
            },
            errors: function (){
                console.log('error')
            }
        });
    });
});