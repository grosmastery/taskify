$(document).ready(function(){
    $('.delete-btn').click(function(){
        let goal_id = $(this).data('id');
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let url = $(this).attr("action");

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'goal_id': goal_id,
                csrfmiddlewaretoken: token,
            },
            success: function (response) {
                if (response.status == 'success') {
                    window.location.reload();
                }
            }
        });
    });
});