$(document).ready(function() {
    $('.goal').click(function(){
        let goal_id = $(this).data('goal_id');
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let url = $('#div-card-body').attr('action');

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'goal_id': goal_id,
                csrfmiddlewaretoken: token,
            },
            success: function (respond) {
                if(respond['status'] == 'success'){
                    window.location.reload(true);
                }
            },
            error: function () {
                console.log('not ok')
            }
        });
    });
});
