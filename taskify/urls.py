from django.contrib import admin
from django.urls import path
from core import views
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
                  path('logout/', auth_views.LogoutView.as_view(), name='logout'),
                  path('signup/', views.SignUpView.as_view(), name='signup'),
                  path('', views.MainView.as_view()),
                  path('todolist/', views.ToDoListView.as_view(), name='todo_list'),
                  path('todolist/<int:pk>/', views.ToDoListDetailView.as_view(), name='todo_list_detail'),
                  path('todotitle/', views.CreateToDoListView.as_view(), name='task_title'),
                  path('creategoal/<int:id>/', views.CreateGoalsView.as_view(), name='create_goal'),
                  path('delete/', views.DeleteGoalView.as_view(), name='delete'),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
